﻿using proj_zakAPI.DBHelper;
using proj_zakAPI.Helper;
using proj_zakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proj_zakAPI.Controllers
{
    public class LoginController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        public Response<User> post(Request<UserLogin> request)
        {
            var result = DBUser.UserLogin(new User { Email = request.parm.UserName, Phone = request.parm.UserName, Password = request.parm.Password });
            if (result.Status)
            {
                result.Token = JwtManager.GenerateToken(result.Item.UserId.ToString(), Util.GetTokenExpirationTimeInMins());
            }
            return result;

        }


    }
}
