
using proj_zakAPI.DBHelper;
using proj_zakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proj_zakAPI.Controllers
{
    public class ItemController : ApiController
    {
        [JwtAuthentication]
        [HttpPost]
        public Response<ItemCategory> post(Request<ItemCategory> request)
        {
            var result = new Response<ItemCategory>();
            if (request.MethodType == ActionType.category)
            {
                var dbScanResult = DBItem.GetCategoryList(request);

                result.Items = dbScanResult.Items;

                result.Status = dbScanResult.Status;
                result.StatusCode = dbScanResult.StatusCode;
                result.Message = dbScanResult.Message;
            }
            
            return result;

        }
    }
}
