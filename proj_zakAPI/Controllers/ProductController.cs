﻿using proj_zakAPI.DBHelper;
using proj_zakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proj_zakAPI.Controllers
{
    public class ProductController : ApiController
    {
        [JwtAuthentication]
        [HttpPost]
        public Response<Product> post(Request<Product> request)
        {
            var result = new Response<Product>();
            var dbScanResult = new Response<Product>();
            if (request.MethodType == ActionType.create && request.parm.ProductId <= 0)
            {
                dbScanResult = DBProduct.CreateProduct(request);
            }
            else if (request.MethodType == ActionType.create && request.parm.ProductId > 0)
            {
                dbScanResult = DBProduct.UpdateProduct(request);
            }
            else if (request.MethodType == ActionType.get || request.MethodType == ActionType.defaultVal)
            {
                dbScanResult = DBProduct.GetProduct(request);
            }

            result.Item = dbScanResult.Item;
            result.Items = dbScanResult.Items;
            result.Status = dbScanResult.Status;
            result.StatusCode = dbScanResult.StatusCode;
            result.Message = dbScanResult.Message;

            return result;

        }
    }
}
