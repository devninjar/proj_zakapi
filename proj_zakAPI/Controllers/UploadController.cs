﻿using proj_zakAPI.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proj_zakAPI.Controllers
{
    public class UploadController : ApiController
    {
        [HttpPost()]
        public string UploadFiles(int path)
        {
            int iUploadedCnt = 0;

            try
            {

                // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.

                string thumbnailPath = System.Web.Hosting.HostingEnvironment.MapPath(ConfigSetting.ImageThumbnailFolderPath);
                string imagedPath = System.Web.Hosting.HostingEnvironment.MapPath(ConfigSetting.ImageUploadFolderPath);

                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

                if (!Directory.Exists(imagedPath))
                    Directory.CreateDirectory(imagedPath);
                if (!Directory.Exists(thumbnailPath))
                    Directory.CreateDirectory(thumbnailPath);
                var fileNameList = new List<string>();

                var baseUrl = Url.Content(ConfigSetting.ImageUploadFolderPath);

                // CHECK THE FILE COUNT.
                for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
                {

                    System.Web.HttpPostedFile hpf = hfc[iCnt];

                    if (hpf.ContentLength > 0)
                    {
                        var fileName = string.Format("{1}_{0}", Path.GetFileName(hpf.FileName), DateTime.Now.ToFileTime());
                        // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                        if (!File.Exists(imagedPath + fileName))
                        {

                            // SAVE THE FILES IN THE FOLDER.
                            hpf.SaveAs(imagedPath + fileName);
                            SaveThumbnailImage(fileName, imagedPath, thumbnailPath);
                            iUploadedCnt = iUploadedCnt + 1;
                            if (path == 1)
                                fileName = baseUrl + fileName;
                            fileNameList.Add(fileName);
                        }
                    }
                }

                // RETURN A MESSAGE.
                if (iUploadedCnt > 0)
                {
                    return string.Join(",", fileNameList);
                }
                else
                {
                    return iUploadedCnt.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        static void SaveThumbnailImage(string imageName, string imagePath, string thumbnailPath)
        {
           
                // Load image.
                Image image = Image.FromFile(imagePath + imageName);

                // Compute thumbnail size.
                Size thumbnailSize = GetThumbnailSize(image);

                // Get thumbnail.
                Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                    thumbnailSize.Height, null, IntPtr.Zero);

                // Save thumbnail.
                thumbnail.Save(thumbnailPath + imageName);
           
        }

        static Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            int maxPixels = ConfigSetting.ImageThumbnailSizeInPixel;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }
    }

}

