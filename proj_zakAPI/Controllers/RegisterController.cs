﻿using proj_zakAPI.DBHelper;
using proj_zakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace proj_zakAPI.Controllers
{

    public class RegisterController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        public Response<User> post(Request<User> request)
        {
            var result = new Response<User>();
            if (request.MethodType == ActionType.register)
            {
                result = DBUser.UserRegster(request.parm);

                return result;
            }
            else if (request.MethodType == ActionType.verify && !string.IsNullOrEmpty(request.parm.Phone) && request.parm.UserId > 0)
            {
                if (request.parm.IsPhoneVerified != null && request.parm.IsPhoneVerified.Value)
                {
                    result = DBUser.VerifieMobile(request.parm);

                    return result;
                }
                else
                {
                    result.Item = request.parm;
                    result.Item.OTP = 123;//TODO: replace with sms getway to send otp;
                    result.Status = true;
                    result.Message = "OTP send to the given mobile.";

                    return result;
                }
            }
            else
                throw new HttpResponseException(HttpStatusCode.Conflict);
        }
    }
}
