﻿using proj_zakAPI.DBHelper;
using proj_zakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proj_zakAPI.Controllers
{
    [JwtAuthentication]
    public class ChatController : ApiController
    {
        
        [HttpPost]
        public Response<Chat> post(Request<Chat> request)
        {
            var result = new Response<Chat>();
            if (request.MethodType == ActionType.scan)
            {
                var dbScanResult = DBChat.PhoneScan(request.parm.PhoneNumber);
                result.Item = new Chat();
                result.Item.PhoneNumber = dbScanResult.Item;

                result.Status = dbScanResult.Status;
                result.StatusCode = dbScanResult.StatusCode;
                result.Message = dbScanResult.Message;
            }
            else if (request.MethodType == ActionType.create)
            {
                var dbcChatResult = DBChat.CreateChatConversation(request.parm);
                result = dbcChatResult;
            }
            return result;

        }
    }
}
