

declare @xml1 xml ='<ROOT><Customers><Customer CustomerName="Arshad Ali" CustomerID="C001"><Orders><Order OrderDate="2012-07-04T00:00:00" OrderID="10248"><OrderDetail Quantity="5" ProductID="10"/><OrderDetail Quantity="12" ProductID="11"/><OrderDetail Quantity="10" ProductID="42"/></Order></Orders><Address> Address line 1, 2, 3</Address></Customer><Customer CustomerName="Paul Henriot" CustomerID="C002"><Orders><Order OrderDate="2011-07-04T00:00:00" OrderID="10245"><OrderDetail Quantity="12" ProductID="11"/><OrderDetail Quantity="10" ProductID="42"/></Order></Orders><Address> Address line 5, 6, 7</Address></Customer><Customer CustomerName="Carlos Gonzlez" CustomerID="C003"><Orders><Order OrderDate="2012-08-16T00:00:00" OrderID="10283"><OrderDetail Quantity="3" ProductID="72"/></Order></Orders><Address> Address line 1, 4, 5</Address></Customer></Customers></ROOT>'
DECLARE @DocHandle int 

------TYPE 1-----------------
SELECT
      Customer.value('@CustomerID','VARCHAR(100)') AS Id, --ATTRIBUTE
      Customer.value('(@CustomerName)[1]','VARCHAR(100)') AS Name --TAG
      --,Customer.value('(Country/text())[1]','VARCHAR(100)') AS Country --TAG
      FROM
      @xml1.nodes('/ROOT/Customers/Customer')AS asd(Customer)
 
------TYPE 3-----------------
	   EXEC sp_xml_preparedocument @DocHandle OUTPUT, @xml1 
SELECT CustomerID, CustomerName, Address
FROM OPENXML(@DocHandle, 'ROOT/Customers/Customer')
WITH 
(
CustomerID [varchar](50) '@CustomerID',
CustomerName [varchar](100) '@CustomerName',
Address [varchar](100) 'Address'
)

EXEC sp_xml_removedocument @DocHandle

------TYPE 3-----------------
EXEC sp_xml_preparedocument @DocHandle OUTPUT, @xml1

SELECT CustomerID, CustomerName, Address, OrderID, OrderDate
FROM OPENXML(@DocHandle, 'ROOT/Customers/Customer/Orders/Order')
WITH 
(
CustomerID [varchar](50) '../../@CustomerID',
CustomerName [varchar](100) '../../@CustomerName',
Address [varchar](100) '../../Address',
OrderID [varchar](1000) '@OrderID',
OrderDate datetime '@OrderDate'
)

EXEC sp_xml_removedocument @DocHandle
GO