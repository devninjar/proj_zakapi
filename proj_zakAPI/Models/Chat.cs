using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.Models
{
    public class Chat
    {
        public long UserAId { get; set; }
        public long UserBId { get; set; }
        public long ConversationId { get; set; }
        public DateTime DateCreated { get; set; }
        public List<ChatConversation> Conversation { get; set; }
        public PhoneNumber PhoneNumber { get; set; }
        public Chat()
        {
            Conversation = new List<Models.ChatConversation>();
            PhoneNumber = new Models.PhoneNumber();
        }


    }

    public class ChatConversation
    {
        public long ConversationId { get; set; }
        public long Id { get; set; }
        public string TextEng { get; set; }
        public string TextChinese { get; set; }
        public DateTime TimeStamp { get; set; }
        public long FromUserID { get; set; }
        public long ToUserID { get; set; }
        public bool IsRead { get; set; }
    }

    public class ScanPhone
    {
        public string PhoneNumber { get; set; }
        public long UserId { get; set; }
        public string Name { get; set; }
    }

    public class PhoneNumber
    {
        public List<ScanPhone> AllPhoneNumber { get; set; }
        public PhoneNumber()
        {
            AllPhoneNumber = new List<ScanPhone>();
        }
    }
}
