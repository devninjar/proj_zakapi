﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI
{
    /// <summary>
    /// Custome message
    /// </summary>
    public class CustomMesg
    {
        /// <summary>
        /// Login successfully
        /// </summary>   
        public static string messge1
        {
            get
            {
                return "Login successfully";
            }

        }

        /// <summary>
        /// Invalid Token
        /// </summary>   
        public static string messge2
        {
            get
            {
                return "Invalid Token";
            }

        }
    }
}