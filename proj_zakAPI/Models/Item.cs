﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.Models
{
    public class ItemCategory
    {
        public int CategoryId { get; set; }
        public int ParentCategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool HaveChild { get; set; }
    }
}