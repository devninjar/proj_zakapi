﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.Models
{
    public class UserLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
    }

    public class User
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string DeviceId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool? IsPhoneVerified { get; set; }
        public bool? IsActive { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool? IsLogind { get; set; }
        public bool? IsFirst { get; set; }
        public string DeviceType { get; set; }
        public int OTP { get; set; }
        public string Country { get; set; }
        public bool IsSeller { get; set; }
    }
}