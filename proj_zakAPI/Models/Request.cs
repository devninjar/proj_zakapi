
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.Models
{
    public class Request<T>
    {
        public Request()
        {
            MethodType = ActionType.defaultVal;
            PageNo = 1;
            PageSize = 10;
        }

        public ActionType MethodType { get; set; }

        public T parm { get; set; }
        
        public string LanguageCode { get; set; }

        public int UserId { get; set; }

        public int PageNo { get; set; }

        public int PageSize { get; set; }

    }


}
