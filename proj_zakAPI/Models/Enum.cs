using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI
{

    public enum ActionType
    {
        defaultVal = 0,
        insert = 1,
        update = 2,
        delete = 3,
        detail = 4,
        get = 5,
        register = 6,
        verify = 7,
        create = 8,
        scan = 9,
        category = 10
    }

}
