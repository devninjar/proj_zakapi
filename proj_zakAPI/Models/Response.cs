﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.Models
{
    public class Response<T>
    {
        public Response()
        {            
            Items = new List<T>();
          
        }       

        public T Item { get; set; }

        public List<T> Items { get; set; }

        public bool Status { get; set; }

        public int StatusCode { get; set; }

        public string Message { get; set; }

        public string Token { get; set; }
    }
}