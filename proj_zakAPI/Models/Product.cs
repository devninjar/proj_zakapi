﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Barcode { get; set; }
        public int Quntity { get; set; }
        public decimal MRP { get; set; }
        public decimal MRPOffer { get; set; }
        public decimal SellingPrice { get; set; }
        public string Unit { get; set; }
        public string ImagesName { get; set; }
        public DateTime UpdateDate { get; set; }

    }
}