﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace proj_zakAPI.Helper
{
    public static class ConfigSetting
    {
        public static int TokenExpireTimeInHrs
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["TokenExpireTimeInHrs"]);
            }

        }

        public static int TokenExpireTimeInDays
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["TokenExpireTimeInDays"]);
            }

        }

        public static string ImageUploadFolderPath
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageUploadFolderPath"];
            }
        }

        public static string ImageThumbnailFolderPath
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageThumbnailFolder"];
            }
        }

        public static int ImageThumbnailSizeInPixel
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["ImageThumbnailSizeInPixel"]);
            }

        }
    }
}