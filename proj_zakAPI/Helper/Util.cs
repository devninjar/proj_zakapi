    ﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.Helper
{
    public class Util
    {
        /// <summary>
        /// Get Total expiration time for authentication Token
        /// </summary>
        /// <returns></returns>
        public static int GetTokenExpirationTimeInMins()
        {
            var totalaMins = 0;

            var hrs = ConfigSetting.TokenExpireTimeInHrs;
            var days = ConfigSetting.TokenExpireTimeInDays;
            totalaMins = (hrs * 60) + (days * 60 * 0);

            return totalaMins;
        }

       //protected void Translate(object sender, EventArgs e)
       // {
       //     string url = "https://translation.googleapis.com/language/translate/v2?key=YOUR_API_KEY";
       //     url += "&source=" + ddlSource.SelectedItem.Value;
       //     url += "&target=" + ddlTarget.SelectedItem.Value;
       //     url += "&q=" + web.UrlEncode(txtSource.Text.Trim());
       //     WebClient client = new WebClient();
       //     string json = client.DownloadString(url);
       //     JsonData jsonData = (new JavaScriptSerializer()).Deserialize<JsonData>(json);
       //     txtTarget.Text = jsonData.Data.Translations[0].TranslatedText;
       // }

        public class JsonData
        {
            public Data Data { get; set; }
        }
        public class Data
        {
            public List<Translation> Translations { get; set; }
        }
        public class Translation
        {
            public string TranslatedText { get; set; }
        }

        
    }
}
