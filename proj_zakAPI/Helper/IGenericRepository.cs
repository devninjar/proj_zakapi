﻿using System.Collections.Generic;
using System;

namespace proj_zakAPI.Helper
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> List { get; }
        void Add(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
        TEntity FindById(int Id);
    }




}