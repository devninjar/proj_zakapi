using proj_zakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.DBHelper
{
    public class DBChat
    {
        public static Response<PhoneNumber> PhoneScan(PhoneNumber phoneNos)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));


            var result = new Response<PhoneNumber>();
            var xml = string.Empty;
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_scan_phone_for_userId
                        (
                            xml,
                            outputStatus,
                            outputMsg
                        );
            }

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }

        public static Response<Chat> CreateChatConversation(Chat chat)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));
            var outputXml = new System.Data.Entity.Core.Objects.ObjectParameter("xmlout", typeof(string));


            var result = new Response<Chat>();
            var xml = string.Empty;
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_create_chat_conversation
                        (
                            chat.UserAId,
                            chat.UserBId,
                            chat.DateCreated,
                            outputXml,
                            outputStatus,
                            outputMsg
                        );
            }
            var obj = Convert.ToString(outputXml.Value);

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }
    }
}
