﻿using System;
using proj_zakAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.DBHelper
{
    public class DBItem
    {
         public static Response<ItemCategory> GetCategoryList(Request<ItemCategory> cate)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));


            var result = new Response<ItemCategory>();
            var xml = string.Empty;
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_get_category
                        (
                            cate.parm.CategoryId,
                            cate.LanguageCode,
                            outputStatus,
                            outputMsg
                        );
                result.Items = rs.Select(x => new ItemCategory
                {
                    CategoryId = x.CategoryId,
                    Description = x.Description,
                    HaveChild = x.IsChild,
                    Name = x.Name,
                    ParentCategoryId = x.ParentCatId,
                    UpdateDate = x.UpdatedDate.Value
                }).ToList();
            }

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }
    }
}
