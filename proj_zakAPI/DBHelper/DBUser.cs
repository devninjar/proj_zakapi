﻿using proj_zakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace proj_zakAPI.DBHelper
{
    public class DBUser
    {

        public static Response<User> UserRegster(User user)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));
            var outpuUserId = new System.Data.Entity.Core.Objects.ObjectParameter("userId", typeof(Int64));

            var result = new Response<User>();
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_create_user
                        (
                            user.FullName,
                            user.Email,
                            user.Password,
                            user.Country,
                            user.IsSeller,
                            outpuUserId,
                            outputStatus,
                            outputMsg
                        );
            }
            result.Item = new Models.User();
            result.Item.UserId = Convert.ToInt64(outpuUserId.Value);
            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }

        public static Response<User> VerifieMobile(User user)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));

            var result = new Response<User>();
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_phone_verified(user.UserId, user.Phone, outputStatus, outputMsg);
            }

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }

        public static Response<User> UserLogin(User user)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));
            var outpuUserId = new System.Data.Entity.Core.Objects.ObjectParameter("userId", typeof(Int64));
            var userDetail = new sp_get_user_detail_Result();
            var result = new Response<User>();
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_user_login(
                            user.Email,
                            user.Phone,
                            user.Password,
                            outpuUserId,
                            outputStatus,
                            outputMsg
                        );

            }

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);
            var userId = Convert.ToInt64(outpuUserId.Value);
            if (result.Status && userId > 0)
                using (var dbContext = new Zakhira_testDBEntities())
                {
                    userDetail = dbContext.sp_get_user_detail(userId, outputStatus, outputMsg).FirstOrDefault();
                    result.Item = new Models.User
                    {
                        Email = userDetail.Email,
                        Phone = userDetail.Phone,
                        UserId = userDetail.UserId,
                        FullName = userDetail.Name,
                        LastLogin = userDetail.LastLogin.Value,
                        CreatedDate = userDetail.CreatedDate.Value,
                        IsFirst = userDetail.IsFirst,
                        IsActive = userDetail.IsActive,
                        IsPhoneVerified = userDetail.IsPhoneVerified
                    };
                }
            return result;
        }

        public static Response<User> UpdateUserDetails(User user)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));

            var result = new Response<User>();
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_update_user_detail
                    (
                         user.FullName,
                         user.Phone,
                         user.Password,
                         user.UserId,
                         user.IsPhoneVerified,
                         user.IsActive,
                         user.IsLogind,
                         user.IsFirst,
                         outputStatus, outputMsg
                    );
            }

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }
    }
}