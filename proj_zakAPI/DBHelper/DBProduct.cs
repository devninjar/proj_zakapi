﻿using proj_zakAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proj_zakAPI.DBHelper
{
    public class DBProduct
    {
        public static Response<Product> CreateProduct(Request<Product> prd)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));
            var outputProductId = new System.Data.Entity.Core.Objects.ObjectParameter("productId", typeof(Int32));

            var result = new Response<Product>();
            var xml = string.Empty;
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_create_product
                        (
                            prd.parm.Name,
                            prd.parm.Description,
                            prd.parm.Barcode,
                            prd.LanguageCode,
                            prd.parm.Quntity,
                            prd.parm.MRP,
                            prd.parm.MRPOffer,
                            prd.parm.SellingPrice,
                            prd.parm.Unit,
                            prd.parm.ImagesName,
                            prd.UserId,
                            prd.parm.CategoryId,
                            outputProductId,
                            outputStatus,
                            outputMsg
                        );
                result.Item = new Product();
                result.Item.ProductId = Convert.ToInt32(outputProductId.Value);
            }

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }

        public static Response<Product> UpdateProduct(Request<Product> prd)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));

            var result = new Response<Product>();
            var xml = string.Empty;
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_update_product
                        (
                            prd.parm.Name,
                            prd.parm.Description,
                            prd.parm.Barcode,
                            prd.LanguageCode,
                            prd.parm.Quntity,
                            prd.parm.MRP,
                            prd.parm.MRPOffer,
                            prd.parm.SellingPrice,
                            prd.parm.Unit,
                            prd.parm.ImagesName,
                            prd.UserId,
                            prd.parm.CategoryId,
                            prd.parm.ProductId,
                            outputStatus,
                            outputMsg
                        );

                result.Item = new Product();
            }

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }

        public static Response<Product> GetProduct(Request<Product> prd)
        {
            var outputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(Int32));
            var outputMsg = new System.Data.Entity.Core.Objects.ObjectParameter("mssg", typeof(string));


            var result = new Response<Product>();
            var xml = string.Empty;
            using (var dbContext = new Zakhira_testDBEntities())
            {
                var rs = dbContext.sp_get_product
                        (
                            prd.parm.ProductId,
                            prd.parm.CategoryId,
                            prd.LanguageCode,
                            prd.PageNo,
                            prd.PageSize,
                            outputStatus,
                            outputMsg
                        );
                
                result.Items = rs.Select(x => new Product
                {
                    ProductId = x.ProductID.Value,
                    Barcode = x.Barcode,
                    CategoryId = x.CategoryId.Value,
                    Description = x.Description,
                    ImagesName = x.ImagesName,
                    MRP = x.MRP.Value,
                    MRPOffer = x.OfferMRP.Value,
                    Name = x.Name,
                    Quntity = x.Quantity.Value,
                    SellingPrice = x.SellingPrice.Value,
                    Unit = x.Units,
                    //  UpdateDate=x.d


                }).ToList();
            }

            result.StatusCode = Convert.ToInt32(outputStatus.Value);
            result.Status = result.StatusCode == 1;
            result.Message = Convert.ToString(outputMsg.Value);

            return result;
        }
    }
}
